import obspython as obs
import os.path, time
import random


# Constants
BROWSER_NAME = "browser"
BROWSER_SOURCE = "browser_source"
ROTATION_TIME_NAME = "rotation_time"
PATH_NAME = "path"
RANDOM_NAME = "random"


browser_source_name = ""
last_rotation = 0
rotation_time = 0
global_time = 0
url_list = []
last_modified = time.ctime(0)
index = 0
file_path = ""
random_on = False


def script_description():
    return "Rotates from a list of urls in a browser source"


def script_update(settings):
    global rotation_time
    global browser_source_name
    global file_path
    global random_on

    rotation_time = obs.obs_data_get_double(settings, ROTATION_TIME_NAME)
    browser_source_name = obs.obs_data_get_string(settings, BROWSER_NAME)
    random_on = obs.obs_data_get_bool(settings, RANDOM_NAME)
    file_path = obs.obs_data_get_string(settings, PATH_NAME)
    update_url_list()


def update_url_list():
    global last_modified

    if not os.path.isfile(file_path):
        url_list.clear()
        return

    last_modified = time.ctime(os.path.getmtime(file_path))
    text_file = open(file_path)
    url_list.clear()
    for line in text_file.readlines():
        line = str.strip(line)
        if line != "":
            url_list.append(line)
    text_file.close()
    shuffle_list()


def shuffle_list():
    global index

    if random_on:
        random.shuffle(url_list)
    index = 0


def script_tick(seconds):
    global global_time
    global last_rotation
    global index

    if not os.path.isfile(file_path):
        return

    date_modified = time.ctime(os.path.getmtime(file_path))
    if date_modified > last_modified:
        update_url_list()

    global_time += seconds
    if url_list is None or len(url_list) == 0:
        return

    if global_time - last_rotation > rotation_time:
        last_rotation = global_time
        url = url_list[index]
        index += 1
        if index >= len(url_list):
            shuffle_list()

        browser = obs.obs_get_source_by_name(browser_source_name)
        if browser is None:
            return

        settings = obs.obs_data_create()
        obs.obs_data_set_string(settings, "url", url)
        print(f"Changing url in {browser_source_name} to: {url}")
        obs.obs_source_update(browser, settings)
        obs.obs_data_release(settings)


def script_defaults(settings):
    obs.obs_data_set_default_bool(settings, RANDOM_NAME, True)
    obs.obs_data_set_default_double(settings, ROTATION_TIME_NAME, 30)


def source_select(props, sources, prop_name: str, label: str, id_filter: [str]):
    p = obs.obs_properties_add_list(props, prop_name, label,
                                    obs.OBS_COMBO_TYPE_EDITABLE, obs.OBS_COMBO_FORMAT_STRING)
    if sources is not None:
        for source in sources:
            source_id = obs.obs_source_get_unversioned_id(source)
            if source_id in id_filter:
                name = obs.obs_source_get_name(source)
                obs.obs_property_list_add_string(p, name, name)


def script_properties():
    props = obs.obs_properties_create()

    sources = obs.obs_enum_sources()
    source_select(props, sources, BROWSER_NAME, "Browser", [BROWSER_SOURCE])
    obs.obs_properties_add_float(props, ROTATION_TIME_NAME, "Rotation time", 2, 999, 1)
    obs.obs_properties_add_path(props, PATH_NAME, "Source file", obs.OBS_PATH_FILE, "*.txt", None)
    obs.obs_properties_add_bool(props, RANDOM_NAME, "Random")
    obs.source_list_release(sources)

    return props
